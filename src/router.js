import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import Education from './views/Education.vue';
import About from './views/About.vue';
import DetailEducation from './views/DetailEducation.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/education',
      name: 'education',
      component: Education
    },
    {
      path: '/about',
      name: 'about',
      component: About
    },
    {
      path: '/detailEducation',
      name: 'detail-education',
      component: DetailEducation
    }
    
  ]
})
